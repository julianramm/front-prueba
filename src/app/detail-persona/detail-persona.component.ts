import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PersonaService } from '../persona.service';
import { Persona } from '../persona';

@Component({
  selector: 'app-detail-persona',
  templateUrl: './detail-persona.component.html',
  styleUrls: ['./detail-persona.component.css']
})
export class DetailPersonaComponent implements OnInit {

  id: number;
  persona: Persona;
  constructor(private route: ActivatedRoute,private router: Router,
    private personaService: PersonaService) { }

  ngOnInit(): void {
    this.persona = new Persona();
    this.id = this.route.snapshot.params['id'];

    this.personaService.getPersona(this.id).subscribe(data =>{
      console.log(data)
      this.persona = data;

    },
    error => console.log(error));
  }

  list(){
    this.router.navigate(['personas']);
  }
}

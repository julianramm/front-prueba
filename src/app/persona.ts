export class Persona{
    id_persona: number;
    nombre: string;
    apellido: string;
    edad: number;
    sexo: string;
    correo: string;
}

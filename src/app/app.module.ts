import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { PersonaService } from './persona.service';
import { CreatePersonaComponent } from './create-persona/create-persona.component';
import { ListPersonaComponent } from './list-persona/list-persona.component';
import { DetailPersonaComponent } from './detail-persona/detail-persona.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { UpdatePersonaComponent } from './update-persona/update-persona.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path: '', redirectTo: 'personas', pathMatch: 'full' },
  {path: 'personas', component: ListPersonaComponent},
  {path: 'add', component: CreatePersonaComponent},
  {path: 'update/:id', component: UpdatePersonaComponent},
  {path: 'details/:id', component: DetailPersonaComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    CreatePersonaComponent,
    ListPersonaComponent,
    DetailPersonaComponent,
    UpdatePersonaComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(routes)

  ],
  providers: [PersonaService],
  bootstrap: [AppComponent]
})
export class AppModule { }

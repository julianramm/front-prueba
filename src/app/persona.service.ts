import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PersonaService {
  private baseUrl = 'http://localhost:9191';

  constructor(private http: HttpClient) { }

  getPersona(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/persona/${id}`);
  }
  createPersona(persona: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}/addPersona`, persona);
  }
  updatePersona( value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/update`, value);
  }
  deletePersona(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/delete/${id}`);
  }
  getPersonas(): Observable<any> {
    return this.http.get(`${this.baseUrl}/personas`);
  }

}

import { Component, OnInit, ComponentFactoryResolver } from '@angular/core';
import { Persona } from '../persona';
import { PersonaService } from '../persona.service';
import { Router, ActivatedRoute } from '@angular/router';
import { error } from '@angular/compiler/src/util';

@Component({
  selector: 'app-update-persona',
  templateUrl: './update-persona.component.html',
  styleUrls: ['./update-persona.component.css']
})
export class UpdatePersonaComponent implements OnInit {

  id: number;
  persona: Persona;
  constructor(
    private personaService: PersonaService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.persona = new Persona();
    this.personaService.getPersona(this.id).subscribe(data =>{
      console.log(data)
      this.persona = data;
    },
    error => console.log(error));
  }

  updatePersona(){
    this.personaService.updatePersona(this.persona).subscribe(data =>
      console.log(data), error => console.log(error));
      this.persona = new Persona();
      this.gotoList();
  }

  onSubmit(){
    this.updatePersona();
  }

  gotoList(){
    this.router.navigate(['personas']);
  }
}

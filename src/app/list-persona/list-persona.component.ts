import { Component, OnInit } from '@angular/core';
import { Persona } from '../persona';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { PersonaService } from '../persona.service';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-list-persona',
  templateUrl: './list-persona.component.html',
  styleUrls: ['./list-persona.component.css']
})
export class ListPersonaComponent implements OnInit {
  personas: Persona[];
  constructor(
    private personaService: PersonaService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.reloadPersonas();
  }

  reloadPersonas(){
    this.personaService.getPersonas().subscribe(data=>{
      this.personas = data;
      console.log(this.personas);
    })
  }
  deletePersona(id: number){
    this.personaService.deletePersona(id).subscribe(data =>{
      console.log(data);
      this.reloadPersonas();
    },
    error => console.log(error));
    this.reloadPersonas()
  }

  personaDetail(id: number){
    this.router.navigate(['details', id]);
  }
  updatePersona(id: number){
    this.router.navigate(['update', id]);

  }
  listarEdad(){
    var a = this.personas.sort(function (a, b) {
      if (a.edad > b.edad) {
        return 1;
      }
      if (a.edad > b.edad) {
        return -1;
      }
      // a must be equal to b
      return 0;
    });
    this.exportexcel("por_edad.xlsx");
  }
  listarSexo(){
   var a = this.personas.filter(data => data.sexo="F");
   var a = this.personas.filter(data => data.sexo="F");
   this.exportexcel("por_sexo.xlsx");
  }

  exportexcel(fileName): void
  {

     /* table id is passed over here */
     let element = document.getElementById('elementos');
     const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);

     /* generate workbook and add the worksheet */
     const wb: XLSX.WorkBook = XLSX.utils.book_new();
     XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

     /* save to file */
     XLSX.writeFile(wb, fileName);

  }

}
